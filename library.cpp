/*Simple Library Management System*/

#include<string.h>
#include<stdlib.h>
#include<iostream>
using namespace std ;
class library
{
    struct details{
        int status,acc_no;
        char std_name[20],usn[20],author[20],bookname[20];
    }d[20];
    int nb;
    public:
    library (int x)
    {
        nb=x;
        for(int i=0;i<nb;i++)
        {
            d[i].status=1;
            strcpy(d[i].usn,"****");
            strcpy(d[i].std_name,"****");
        }
    }
    void read()
    {
        for(int i=0;i<nb;i++)
        {
            cout<<"-----------------------------"<<endl;
            cout<<"Enter the book name:";
            cin>>d[i].bookname;
            cout<<"Enter the author name:";
            cin>>d[i].author;
            cout<<"Enter the accersion number:";
            cin>>d[i].acc_no;
        }
    }
    void issue(int i)
    {
        char std_name1[20],usn1[20];
        cout<<"Enter the student name who is receiving the book:";
        cin>>std_name1;
        cout<<"Say me your USN number:";
        cin>>usn1;
        d[i].status=0;
        strcpy(d[i].std_name,std_name1);
        strcpy(d[i].usn,usn1);
        cout<<"Book is Issued"<<endl;
    }
    void return_book(int i)
    {
        d[i].status=1;
        strcpy(d[i].std_name,"****");
        strcpy(d[i].usn,"****");
        cout<<"Book is Collected back";
    }   
    void display()
    {
        int i;
        for(i=0;i<nb;i++)
        {
            cout<<"-----------------------------------------------------------------------"<<endl;
            cout<<d[i].acc_no<<"\t"<<d[i].bookname<<"\t\t"<<d[i].author<<"\t"<<d[i].status<<"\t"<<d[i].std_name<<"\t\t"<<d[i].usn<<endl;
        }
    }
    int search()
    {
        char bname[20];
        cout<<"Enter the name of the book:";
        cin>>bname;
        for(int i=0;i<nb;i++)
        {
            if(strcmp(bname,d[i].bookname)==0)
            {
                cout<<"Book is Available:"<<endl;   
                return (i);
            }
        }
        return -1;
    }
};
int main()
{
    library l(3);
    int ch,x,n;
    char bname[20];
    l.read();
    cout<<"-------------------------------------------------------------------------------"<<endl;
    cout<<"acc_no\t"<<"bookname\t"<<"author\t"<<"status\t"<<"student_name\t"<<"USN"<<endl;
    l.display();
    while(1)
    {
        cout<<"Enter the choice"<<endl<<"1.issue\t2.return\t3.display\t4.exit\n";
        cin>>ch;
        switch(ch)
        {
            case 1: x=l.search();
                    l.issue(x);
                    break;
            case 2: x=l.search();
                    l.return_book(x);
                    break;
            case 3: cout<<"acc_no\t"<<"bookname\t"<<"author\t"<<"status\t"<<"student_name \t"<<"USN"<<endl;
                    l.display();
                    break;
            case 4: exit(1);
        }
    }
    return 0;
}