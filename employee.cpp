/*Employee Salary Calculation*/

#include<string.h>
#include<stdlib.h>
#include<iostream.h>
class employee
{
    char E_no[20],E_name[20];
    float basic,da,it,net_sal,gross_sal;
    public: :
    void read()
    {
        cout<<"Enter the employee id:";
        cin>>E_no;
        cout<<"Enter the employee name:";
        cin>>E_name;
    }
    void compute(float basic1)
    {
        basic=basic1;
        da=(51*basic)/100;
        gross_sal=basic+da;
        it=(20*gross_sal)/100;
        net_sal=gross_sal-it;
    }
    void display()
    {
        cout<<E_no<<"\t\t"<<E_name<<"\t"<<basic<<"\t"<<da<<"\t"<<it<<"\t"<<gross_sal<<"\t\t"<<net_
        sal<<endl;
    }
    int search(char E_name1[20])
    {
        if(strcmp(E_name1,E_name)==0)
        return 1;
        return 0;
    }
};
void main()
{
    employee E[100];
    int n,j,flag;
    char E_name1[20];
    float basic,basic1;
    cout<<"Enter the number of employees:";
    cin>>n;
    for(int i=1;i<=n;i++)
    {
        E[i].read();
        cout<<"Enter the basic:";
        cin>>basic;
        E[i].compute(basic);
    }
    cout<<"The employee details of income dept:"<<endl;
    cout<<"Employee no \t"<<"name \t"<<"basic\t"<<"DA\t"<<"IT\t"<<"Gross salary\t"<<"Netsalary"<<endl;
    for(int i=1;i<=n;i++)
    E[i].display();
    cout<<"Enter the Employee name to modify:"; 
    cin>>E_name1;
    flag=0;
    for(j=1;j<=n;j++)
    {
        if(E[j].search(E_name1))
        {
            flag=1;
            break;
        }
    }
    if(flag==1)
    {
        cout<<"Enter the basic salary:";
        cin>>basic1;
        E[j].compute(basic1);
        cout<<"After few modifications finally:"<<endl;
        cout<<"Employee no \t"<<"name \t"<<"basic\t"<<"DA\t"<<"IT\t"<<"Grosssalary\t"<<"Net salary"<<endl;
        for(int k=1;k<=n;k++)
        E[k].display();
    }
    else
    cout<<"Employee not found...!!!";
    getch();
}